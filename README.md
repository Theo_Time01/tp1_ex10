# TP1 Exercice 10

## Objectif
L'objectif de cet exercice est d'effectuer une intégration continue sous gitlab afin de vérifier le bon fonctionnement du projet et l'archivage de Package_Calculator

## Réalisation
On ajoute un fichier .yml permettant de définir une série d'étape d'intégration continue sous gitlab.  
Pour l'archivage, il faut ajouter le chemin de  Package_Calculator dans le fichier setup.py.

## Organisation
même organisation que les excercices précédents


## Gitlab
Le résultat de l'intégration continue est visible dans l'onglet CI_CD/pipeline du projet gitlab, on peut alors voir que les trois étapes et s'ils elles ont bien été éxécutées sans problème.

 
## Ressources
https://gitlab.com/js-ci-training/ci-hero-unitarytest-python
https://gitlab.com/PaulBx/tp1_exo_8/-/tree/master/

